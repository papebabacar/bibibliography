// @flow

export const SET_SEARCH_DATA = 'SET_SEARCH_DATA';
export const SET_SEARCH_TERM = 'SET_SEARCH_TERM';
export const SET_SIMPLE_SEARCH_TERM = 'SET_SIMPLE_SEARCH_TERM';
export const SET_SEARCH_SUGGESTIONS = 'SET_SEARCH_SUGGESTIONS';
export const SET_SEARCH_RESULT = 'SET_SEARCH_RESULT';
export const CLEAR_SEARCH = 'CLEAR_SEARCH';

export function setSearchData(data) {
  return {
    type: SET_SEARCH_DATA,
    data
  };
}

export function setSearchTerm(prop, term) {
  return {
    type: SET_SEARCH_TERM,
    prop,
    term
  };
}

export function setSimpleSearchTerm(term) {
  return {
    type: SET_SIMPLE_SEARCH_TERM,
    term
  };
}

export function setSearchSuggestions(prop, suggestion) {
  return {
    type: SET_SEARCH_SUGGESTIONS,
    prop,
    suggestion
  };
}

export function setSearchResult(result) {
  return {
    type: SET_SEARCH_RESULT,
    result
  };
}

export function clearSearch() {
  return {
    type: CLEAR_SEARCH
  };
}
