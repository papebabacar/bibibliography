import path from 'path';
import { shell } from 'electron';
import pdfview from 'electron-pdf-viewer';

export const loadFile = (file, title) => event => {
  if (event.type === 'click' || event.key === 'Enter') {
    // const opened = shell.openItem(
    //   path.join(process.resourcesPath, `../books/${file}`)
    // );
    // if (!opened) {
    //   const url = new URL('https://invenio.unidep.org/invenio/search');
    //   url.searchParams.append('ln', 'fr');s
    //   url.searchParams.append('p', `'${title}'`);
    //   shell.openExternal(url.href);
    // }
    const pdfurl =
      'https://www.canon.com.cn/products/printer/pixma-fax/images/speedtest.pdf';

    const displayPdfUrl = pdfview.getPdfHtmlURL(pdfurl);
    console.log(displayPdfUrl);

    const options = {
      width: 800,
      height: 600,
      webPreferences: {
        nodeIntegration: false,
        contextIsolation: true
      }
    };
    // If you dont provive options, default options will be use
    const win = pdfview.showpdf(pdfurl, options);
    win.show();
  }
};

export const loadUrl = event => {
  event.preventDefault();
  shell.openExternal(event.target.href);
};
