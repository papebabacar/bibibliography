import { Route, Switch } from 'react-router';

import AdvancedPage from './containers/AdvancedPage';
import App from './containers/App';
import React from 'react';
/* eslint flowtype-errors/show-errors: 0 */
import SearchPage from './containers/SearchPage';
import routes from './constants/routes.json';

export default () => (
  <App>
    <Switch>
      <Route path={routes.ADVANCED} component={AdvancedPage} />
      <Route path={routes.SEARCH} component={SearchPage} />
    </Switch>
  </App>
);
