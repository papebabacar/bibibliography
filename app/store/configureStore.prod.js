// @flow
import { applyMiddleware, createStore } from 'redux';

import { createHashHistory } from 'history';
import rootReducer from '../reducers';
import { routerMiddleware } from 'react-router-redux';
import type { searchStateType } from '../reducers/types';
import thunk from 'redux-thunk';

const history = createHashHistory();
const router = routerMiddleware(history);
const enhancer = applyMiddleware(thunk, router);

function configureStore(initialState?: searchStateType) {
  return createStore(rootReducer, initialState, enhancer);
}

export default { configureStore, history };
