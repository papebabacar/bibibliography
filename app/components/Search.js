// @flow
import React, { PureComponent } from 'react';
import { loadFile, loadUrl } from '../utils';

import { Link } from 'react-router-dom';
import logo from '../assets/img/logo.png';
import routes from '../constants/routes.json';
import styles from './Search.css';
import tableStyles from './Datatable.css';

type Props = {
  result: Array,
  setSimpleSearchTerm: () => void,
  setSearchResult: () => void,
  clearSearch: () => void,
  simpleTerm: string
};

export default class Search extends PureComponent<Props> {
  props: Props;

  componentDidMount() {
    const { clearSearch } = this.props;
    clearSearch();
  }

  onChange = async event => {
    const { setSimpleSearchTerm, setSearchResult } = this.props;
    await setSimpleSearchTerm(event.target.value);
    const { data, simpleTerm } = this.props;
    if (simpleTerm.trim().length >= 3)
      setSearchResult(search(data, simpleTerm));
  };

  render() {
    const { simpleTerm, result } = this.props;
    const searching = simpleTerm.trim().length >= 3;
    return (
      <div className={styles.background}>
        {!searching ? (
          <p className={styles.text}>
            Cette bibliothèque portable propose un accès hors-ligne à une
            collection de documents patrimoniaux issus des pays africains et
            d’autres produits et publiés par l’IDEP, couvrant tous les domaines
            du développement socio-économique du continent africain. L’accès aux
            documents en texte intégral se fait via un moteur de recherche
            autonome en mode simple et avancé. En cas d’impossibilité d’accès au
            document intégral d’une notice, cliquer sur le lien hypertexte
            correspondant dans ladite notice. La bibliothèque portable a pour
            objectif de contourner d’éventuelles difficultés de connectivité qui
            empêcheraient de consulter cette collection via la bibliothèque
            numérique en ligne de l’IDEP disponible sur :{' '}
            <a
              href="https://invenio.unidep.org/invenio//collection/IDEP%20Heritage?ln=fr"
              onClick={loadUrl}
            >
              https://invenio.unidep.org/invenio//collection/IDEP%20Heritage?ln=fr
            </a>
          </p>
        ) : (
          ''
        )}
        <div className={`${styles.content} ${searching && styles.searching}`}>
          <img className={styles.logo} src={logo} alt="logo" />
          <div className={styles.search}>
            <input
              autoFocus
              className={styles.input}
              placeholder="Search / Recherche"
              value={simpleTerm}
              onChange={this.onChange}
              onFocus={event => event.target.select()}
            />
            <button className={styles.button} type="submit">
              <i className="fa fa-search" />
            </button>
          </div>
          <Link className={styles.advanced} to={routes.ADVANCED}>
            Advanced search / Recherche avancée
          </Link>
        </div>
        <div
          className={`${tableStyles.datatable} ${!searching &&
            tableStyles.hidden}`}
        >
          <a
            className={styles.back}
            onClick={() => {
              const { clearSearch } = this.props;
              clearSearch();
            }}
          >
            <i className="fa fa-arrow-left fa-2x" />
          </a>
          <table className={tableStyles.table}>
            <thead className={tableStyles.header}>
              <tr>
                <th className={tableStyles.cell}>ORIGIN / ORIGINE</th>
                <th className={tableStyles.cell}>TITLE / TITRE</th>
                <th className={tableStyles.cell}>AUTHOR / AUTEUR</th>
                <th className={tableStyles.right}>YEAR / ANNEE</th>
              </tr>
            </thead>
            <tbody>
              {result.map(([file, country, title, year, author], index) => (
                <tr
                  key={index}
                  className={tableStyles.row}
                  onClick={loadFile(file, title)}
                  tabIndex={index + 2}
                  onKeyPress={loadFile(file, title)}
                >
                  <td className={tableStyles.cell}>{country}</td>
                  <td className={tableStyles.cell}>{title}</td>
                  <td className={tableStyles.cell}>{author}</td>
                  <td className={tableStyles.right}>{year}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        {!searching ? (
          <p className={styles.text}>
            This portable library provides offline access to a collection of
            heritage documents from African countries and others produced and
            published by IDEP, covering all areas of socio-economic development
            on the African continent. Access to full-text documents is via an
            autonomous search engine in simple and advanced mode. If you are
            unable to access the full-text document of a record, click on the
            corresponding hypertext link in the given record. The portable
            library aims at circumventing possible connectivity difficulties
            that would prevent consulting this collection via the IDEP online
            digital library available on:{' '}
            <a
              href="https://invenio.unidep.org/invenio//collection/IDEP%20Heritage?ln=en"
              onClick={loadUrl}
            >
              https://invenio.unidep.org/invenio//collection/IDEP%20Heritage?ln=en
            </a>
          </p>
        ) : (
          ''
        )}
        {searching ? (
          <p className={tableStyles.center}>
            {result.length > 0
              ? `${result.length} resultat(s)`
              : 'Aucun resultat'}
          </p>
        ) : (
          ''
        )}
        <a
          className={`${styles.more} ${searching && styles.searching}`}
          href="https://invenio.unidep.org/invenio//collection/IDEP%20Heritage?ln=fr"
          onClick={loadUrl}
        >
          www.unidep.org
        </a>
      </div>
    );
  }
}

const search = (data, term) => {
  if (!term) return data;
  // eslint-disable-next-line no-unused-vars
  return data.filter(([_, country, title, year, author]) => {
    const countryCompare = normalize(country).includes(normalize(term));
    const titleCompare = normalize(title).includes(normalize(term));
    const yearCompare = normalize(year).includes(normalize(term));
    const authorCompare = normalize(author).includes(normalize(term));
    return countryCompare || titleCompare || yearCompare || authorCompare;
  });
};

const normalize = string =>
  string
    .toLowerCase()
    .trim()
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '');
