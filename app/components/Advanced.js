// @flow
import React, { PureComponent } from 'react';
import { loadFile, loadUrl } from '../utils';

import Autosuggest from 'react-autosuggest';
import { Link } from 'react-router-dom';
import logo from '../assets/img/logo.png';
import routes from '../constants/routes.json';
import styles from './Advanced.css';
import tableStyles from './Datatable.css';
import theme from './Autocomplete.css';

type Props = {
  data: array,
  result: array,
  setSearchResult: () => void,
  setSearchTerm: () => void,
  setSearchSuggestions: () => void,
  clearSearch: () => void,
  countryTerm: string,
  titleTerm: string,
  authorTerm: string,
  yearTerm: string,
  countrySuggestions: array,
  titleSuggestions: array,
  authorSuggestions: array,
  yearSuggestions: array
};

export default class Advanced extends PureComponent<Props> {
  props: Props;

  componentDidMount() {
    const { clearSearch } = this.props;
    clearSearch();
  }

  onChange = prop => async (event, { newValue }) => {
    const { setSearchTerm, setSearchResult } = this.props;
    await setSearchTerm(prop, newValue);
    const { data, countryTerm, titleTerm, authorTerm, yearTerm } = this.props;
    const searching =
      countryTerm.trim().length >= 3 ||
      titleTerm.trim().length >= 3 ||
      authorTerm.trim().length >= 3 ||
      yearTerm.trim().length >= 3;
    if (searching)
      setSearchResult(
        search(data, { countryTerm, titleTerm, authorTerm, yearTerm })
      );
  };

  onSuggestionsFetchRequested = prop => ({ value }) => {
    const {
      data,
      setSearchSuggestions,
      countryTerm,
      titleTerm,
      authorTerm,
      yearTerm
    } = this.props;
    const terms = {
      countryTerm,
      titleTerm,
      authorTerm,
      yearTerm,
      [`${prop}Term`]: value
    };
    setSearchSuggestions(prop, search(data, terms, null, prop));
  };

  onSuggestionsClearRequested = prop => () => {
    const { setSearchSuggestions } = this.props;
    setSearchSuggestions(prop, []);
  };

  render() {
    const {
      result,
      countrySuggestions,
      titleSuggestions,
      authorSuggestions,
      yearSuggestions,
      countryTerm,
      titleTerm,
      authorTerm,
      yearTerm
    } = this.props;
    const searching =
      countryTerm.trim().length >= 3 ||
      titleTerm.trim().length >= 3 ||
      authorTerm.trim().length >= 3 ||
      yearTerm.trim().length >= 3;
    return (
      <div className={`${styles.background} ${searching && styles.searching}`}>
        <Link className={styles.back} to={routes.SEARCH}>
          <i className="fa fa-arrow-left fa-2x" />
        </Link>
        {!searching ? (
          <p className={styles.text}>
            Cette bibliothèque portable propose un accès hors-ligne à une
            collection de documents patrimoniaux issus des pays africains et
            d’autres produits et publiés par l’IDEP, couvrant tous les domaines
            du développement socio-économique du continent africain. L’accès aux
            documents en texte intégral se fait via un moteur de recherche
            autonome en mode simple et avancé. En cas d’impossibilité d’accès au
            document intégral d’une notice, cliquer sur le lien hypertexte
            correspondant dans ladite notice. La bibliothèque portable a pour
            objectif de contourner d’éventuelles difficultés de connectivité qui
            empêcheraient de consulter cette collection via la bibliothèque
            numérique en ligne de l’IDEP disponible sur :{' '}
            <a
              href="https://invenio.unidep.org/invenio//collection/IDEP%20Heritage?ln=fr"
              onClick={loadUrl}
            >
              https://invenio.unidep.org/invenio//collection/IDEP%20Heritage?ln=fr
            </a>
          </p>
        ) : (
          ''
        )}
        <img className={styles.logo} src={logo} alt="logo" />
        <div className={styles.toolbar}>
          <Autosuggest
            id="country"
            suggestions={countrySuggestions}
            shouldRenderSuggestions={shouldRenderSuggestions}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested(
              'country',
              1
            )}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested(
              'country'
            )}
            getSuggestionValue={getSuggestionValue(1)}
            renderSuggestion={renderSuggestion(1)}
            inputProps={{
              value: countryTerm,
              onChange: this.onChange('country'),
              onFocus: event => event.target.select()
            }}
            theme={theme}
            renderInputComponent={inputProps => (
              <div className={styles.search}>
                <input
                  autoFocus
                  className={styles.searchInput}
                  type="text"
                  placeholder="Origin / Origine"
                  {...inputProps}
                />
                <button className={styles.button} type="submit">
                  <i className="fa fa-search" />
                </button>
              </div>
            )}
          />
          <Autosuggest
            id="title"
            suggestions={titleSuggestions}
            shouldRenderSuggestions={shouldRenderSuggestions}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested(
              'title',
              2
            )}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested(
              'title'
            )}
            getSuggestionValue={getSuggestionValue(2)}
            renderSuggestion={renderSuggestion(2)}
            inputProps={{
              value: titleTerm,
              onChange: this.onChange('title'),
              onFocus: event => event.target.select()
            }}
            theme={theme}
            renderInputComponent={inputProps => (
              <div className={styles.search}>
                <input
                  className={styles.searchInput}
                  placeholder="Title / Titre"
                  type="text"
                  {...inputProps}
                />
                <button className={styles.button} type="submit">
                  <i className="fa fa-search" />
                </button>
              </div>
            )}
          />
          <Autosuggest
            id="author"
            suggestions={authorSuggestions}
            shouldRenderSuggestions={shouldRenderSuggestions}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested(
              'author',
              4
            )}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested(
              'author'
            )}
            getSuggestionValue={getSuggestionValue(4)}
            renderSuggestion={renderSuggestion(4)}
            inputProps={{
              value: authorTerm,
              onChange: this.onChange('author'),
              onFocus: event => event.target.select()
            }}
            theme={theme}
            renderInputComponent={inputProps => (
              <div className={styles.search}>
                <input
                  className={styles.searchInput}
                  type="text"
                  placeholder="Author / Auteur"
                  {...inputProps}
                />
                <button className={styles.button} type="submit">
                  <i className="fa fa-search" />
                </button>
              </div>
            )}
          />
          <Autosuggest
            id="year"
            suggestions={yearSuggestions}
            shouldRenderSuggestions={shouldRenderSuggestions}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested(
              'year',
              4
            )}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested(
              'year'
            )}
            getSuggestionValue={getSuggestionValue(3)}
            renderSuggestion={renderSuggestion(3)}
            inputProps={{
              value: yearTerm,
              onChange: this.onChange('year'),
              onFocus: event => event.target.select()
            }}
            theme={theme}
            renderInputComponent={inputProps => (
              <div className={styles.search}>
                <input
                  className={styles.searchInput}
                  type="text"
                  placeholder="Year / Année"
                  {...inputProps}
                />
                <button className={styles.button} type="submit">
                  <i className="fa fa-search" />
                </button>
              </div>
            )}
          />
        </div>
        <div
          className={`${styles.result} ${tableStyles.datatable} ${!searching &&
            tableStyles.hidden}`}
        >
          <table className={tableStyles.table}>
            <thead className={tableStyles.header}>
              <tr>
                <th className={tableStyles.cell}>ORIGIN / ORIGINE</th>
                <th className={tableStyles.cell}>TITLE / TITRE</th>
                <th className={tableStyles.cell}>AUTHOR / AUTEUR</th>
                <th className={tableStyles.right}>YEAR / ANNEE</th>
              </tr>
            </thead>
            <tbody>
              {result.map(([file, country, title, year, author], index) => (
                <tr
                  key={index}
                  className={tableStyles.row}
                  onClick={loadFile(file, title)}
                  tabIndex={index + 2}
                  onKeyPress={loadFile(file, title)}
                >
                  <td className={tableStyles.cell}>{country}</td>
                  <td className={tableStyles.cell}>{title}</td>
                  <td className={tableStyles.cell}>{author}</td>
                  <td className={tableStyles.right}>{year}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        {!searching ? (
          <p className={styles.text}>
            This portable library provides offline access to a collection of
            heritage documents from African countries and others produced and
            published by IDEP, covering all areas of socio-economic development
            on the African continent. Access to full-text documents is via an
            autonomous search engine in simple and advanced mode. If you are
            unable to access the full-text document of a record, click on the
            corresponding hypertext link in the given record. The portable
            library aims at circumventing possible connectivity difficulties
            that would prevent consulting this collection via the IDEP online
            digital library available on:{' '}
            <a
              href="https://invenio.unidep.org/invenio//collection/IDEP%20Heritage?ln=en"
              onClick={loadUrl}
            >
              https://invenio.unidep.org/invenio//collection/IDEP%20Heritage?ln=en
            </a>
          </p>
        ) : (
          ''
        )}
        {searching ? (
          <p className={tableStyles.center}>
            {result.length > 0
              ? `${result.length} resultat(s)`
              : 'Aucun resultat'}
          </p>
        ) : (
          ''
        )}
        <a
          className={`${styles.more} ${searching && styles.searching}`}
          href="https://invenio.unidep.org/invenio//collection/IDEP%20Heritage?ln=fr"
          onClick={loadUrl}
        >
          www.unidep.org
        </a>
      </div>
    );
  }
}

const shouldRenderSuggestions = value => value.trim().length >= 3;

const getSuggestionValue = term => data => data[term].toLowerCase().trim();

const renderSuggestion = term => data => data[term];

const search = (data, terms, limit, sort) => {
  // eslint-disable-next-line no-unused-vars
  const result = data.filter(([_, country, title, year, author]) => {
    const countryCompare = normalize(country).includes(
      normalize(terms.countryTerm)
    );
    const titleCompare = normalize(title).includes(normalize(terms.titleTerm));
    const authorCompare = normalize(author).includes(
      normalize(terms.authorTerm)
    );
    const yearCompare = normalize(year).includes(normalize(terms.yearTerm));
    return countryCompare && titleCompare && authorCompare && yearCompare;
  });
  const prop = { country: 1, title: 2, year: 3, author: 4 };
  const noDuplicates = sort
    ? result.reduce(
        (array, resultData) =>
          array.find(
            arrayData =>
              normalize(arrayData[prop[sort]]) ===
              normalize(resultData[prop[sort]])
          )
            ? array
            : [...array, resultData],
        []
      )
    : result;
  const sortedResult = sort
    ? noDuplicates.sort(
        (data1, data2) =>
          normalize(data1[prop[sort]]).indexOf(
            normalize(terms[`${sort}Term`])
          ) >
          normalize(data2[prop[sort]]).indexOf(normalize(terms[`${sort}Term`]))
      )
    : noDuplicates;
  return limit ? sortedResult.slice(0, limit) : sortedResult;
};

const normalize = string =>
  string
    .toLowerCase()
    .trim()
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '');
