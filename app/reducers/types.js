import type { Dispatch as ReduxDispatch, Store as ReduxStore } from 'redux';

export type searchStateType = {
  +data: array
};

export type Action = {
  +type: string
};

export type GetState = () => searchStateType;

export type Dispatch = ReduxDispatch<Action>;

export type Store = ReduxStore<GetState, Action>;
