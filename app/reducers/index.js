// @flow
import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import search from './search';

const rootReducer = combineReducers({
  search,
  router
});

export default rootReducer;
