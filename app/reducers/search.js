import type { Action, searchStateType } from './types';
// @flow
import {
  CLEAR_SEARCH,
  SET_SEARCH_DATA,
  SET_SEARCH_RESULT,
  SET_SEARCH_SUGGESTIONS,
  SET_SEARCH_TERM,
  SET_SIMPLE_SEARCH_TERM
} from '../actions/search';

const initialSate = {
  data: [],
  result: [],
  simpleTerm: '',
  countryTerm: '',
  titleTerm: '',
  authorTerm: '',
  yearTerm: '',
  countrySuggestions: [],
  titleSuggestions: [],
  authorSuggestions: [],
  yearSuggestions: []
};

export default function search(
  state: searchStateType = initialSate,
  action: Action
) {
  switch (action.type) {
    case SET_SEARCH_DATA:
      return { ...state, data: action.data, result: action.data };
    case SET_SEARCH_TERM:
      return { ...state, [`${action.prop}Term`]: action.term };
    case SET_SIMPLE_SEARCH_TERM:
      return { ...state, simpleTerm: action.term };
    case SET_SEARCH_SUGGESTIONS:
      return { ...state, [`${action.prop}Suggestions`]: action.suggestion };
    case SET_SEARCH_RESULT:
      return { ...state, result: action.result };
    case CLEAR_SEARCH:
      return { ...initialSate, data: state.data, result: state.result };
    default:
      return state;
  }
}
