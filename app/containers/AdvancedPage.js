import * as SearchActions from '../actions/search';

import Advanced from '../components/Advanced';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// @flow

function mapStateToProps({
  search: {
    data,
    result,
    countryTerm,
    titleTerm,
    authorTerm,
    yearTerm,
    countrySuggestions,
    titleSuggestions,
    authorSuggestions,
    yearSuggestions
  }
}) {
  return {
    data,
    result,
    countryTerm,
    titleTerm,
    authorTerm,
    yearTerm,
    countrySuggestions,
    titleSuggestions,
    authorSuggestions,
    yearSuggestions
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(SearchActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Advanced);
