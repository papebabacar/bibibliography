import { bindActionCreators } from 'redux';
import * as SearchActions from '../actions/search';

import Search from '../components/Search';
// @flow
import { connect } from 'react-redux';

function mapStateToProps({ search: { data, result, simpleTerm } }) {
  return {
    data,
    result,
    simpleTerm
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(SearchActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
