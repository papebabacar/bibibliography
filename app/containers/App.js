// @flow
import * as React from 'react';
import * as SearchActions from '../actions/search';

import Papa from 'papaparse';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import fs from 'fs';
import path from 'path';

type Props = {
  children: React.Node,
  setSearchData: () => void
};

class App extends React.Component<Props> {
  props: Props;

  componentDidMount() {
    const { setSearchData } = this.props;
    fs.readFile(
      path.join(process.resourcesPath, '../books/database.csv'),
      'utf-8',
      (err, database) => {
        Papa.parse(database.trim(), {
          complete: ({ data }) => setSearchData(data)
        });
      }
    );
  }

  render() {
    const { children } = this.props;
    return <React.Fragment>{children}</React.Fragment>;
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(SearchActions, dispatch);
}

export default connect(
  () => ({}),
  mapDispatchToProps
)(App);
